import {t, Selector} from 'testcafe'

class BasePage {
  baseClasses = {
    pageTitle: '.page__title',
    radar: '.radar',
    radarQuadrantTopRight: '.radar__quadrant-top-right',
    radarQuadrantTopLeft: '.radar__quadrant-top-left',
    radarQuadrantBottomLeft: '.radar__quadrant-bottom-left',
    radarQuadrantBottomRight: '.radar__quadrant-bottom-right',
    radarTitle: '.radar__title',
  }

  baseSelectors = {
    pageTitle: Selector(this.baseClasses.pageTitle),
    radar: Selector(this.baseClasses.radar),
    radarQuadrantTopRight: Selector(this.baseClasses.radarQuadrantTopRight),
    radarQuadrantTopLeft: Selector(this.baseClasses.radarQuadrantTopLeft),
    radarQuadrantBottomLeft: Selector(this.baseClasses.radarQuadrantBottomLeft),
    radarQuadrantBottomRight: Selector(
      this.baseClasses.radarQuadrantBottomRight
    ),
    radarTitle: Selector(this.baseClasses.radarTitle),
  }

  async verifyPageTitleEquals(title: string) {
    await t
      .expect(this.baseSelectors.pageTitle.innerText)
      .eql(title, {timeout: 10000})
  }

  async verifyPageTitleContains(title: string) {
    await t
      .expect(this.baseSelectors.pageTitle.innerText)
      .contains(title, {timeout: 10000})
  }

  async verifyPageTitleMatches(titleRegEx: RegExp) {
    await t
      .expect(this.baseSelectors.pageTitle.innerText)
      .match(titleRegEx, {timeout: 10000})
  }

  async verifyRadarWithTitleExists(title: string) {
    const radar = this.baseSelectors.radar.withText(title)
    await t.expect(radar.exists).ok()
  }

  async verifyRadarHasFourQuadrants(title: string) {
    const radar = this.baseSelectors.radar.withText(title)
    await t
      .expect(radar.find(this.baseClasses.radarQuadrantTopRight).exists)
      .ok()
    await t
      .expect(radar.find(this.baseClasses.radarQuadrantTopLeft).exists)
      .ok()
    await t
      .expect(radar.find(this.baseClasses.radarQuadrantBottomLeft).exists)
      .ok()
    await t
      .expect(radar.find(this.baseClasses.radarQuadrantBottomRight).exists)
      .ok()
  }
}

export default new BasePage()

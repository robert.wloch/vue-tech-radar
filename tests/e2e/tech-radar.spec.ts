import BasePage from './common/BasePage'

fixture`Getting Started`.page`http://localhost:3000/`

test('Shows "Tech Radar" as page title', async () => {
  await BasePage.verifyPageTitleContains('Tech Radar')
})

test('Shows "Adopt" radar with four quadrants', async () => {
  await BasePage.verifyRadarWithTitleExists('ADOPT')
  await BasePage.verifyRadarHasFourQuadrants('ADOPT')
})

test('Shows "Trial" radar with four quadrants', async () => {
  await BasePage.verifyRadarWithTitleExists('TRIAL')
  await BasePage.verifyRadarHasFourQuadrants('TRIAL')
})

test('Shows "Assess" radar with four quadrants', async () => {
  await BasePage.verifyRadarWithTitleExists('ASSESS')
  await BasePage.verifyRadarHasFourQuadrants('ASSESS')
})

test('Shows "Hold" radar with four quadrants', async () => {
  await BasePage.verifyRadarWithTitleExists('HOLD')
  await BasePage.verifyRadarHasFourQuadrants('HOLD')
})

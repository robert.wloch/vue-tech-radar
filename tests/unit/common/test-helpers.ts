import {DOMWrapper, VueWrapper} from '@vue/test-utils'

export function useTestHelpers() {
  return {
    expectHeroComponent,
  }
}

function expectHeroComponent(wrapper: VueWrapper, stubTag: string) {
  expect(heroComponent(wrapper, stubTag).exists()).toBeTruthy()
}

function heroComponent(
  wrapper: VueWrapper,
  stubTag: string
): DOMWrapper<Element> {
  return wrapper.find(stubTag)
}

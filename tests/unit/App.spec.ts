import {flushPromises, mount, VueWrapper} from '@vue/test-utils'
import {ComponentPublicInstance, nextTick} from 'vue'
import {useTestHelpers} from '@unit/common/test-helpers'
import sut from '@/App.vue'

const {expectHeroComponent} = useTestHelpers()

describe('App', () => {
  let wrapper: VueWrapper<ComponentPublicInstance> | null = null

  afterEach(() => {
    if (wrapper) {
      wrapper.unmount()
    }
  })

  it('should mount root app', async () => {
    wrapper = await createWrapper()

    expect(wrapper.exists()).toBeTruthy()
  })

  it('should contain tech radar component', async () => {
    wrapper = await createWrapper()

    expectHeroComponent(wrapper, 'tech-radar-stub')
  })

  async function createWrapper(): Promise<VueWrapper> {
    const vueWrapper = mount(sut, {
      global: {
        stubs: {
          TechRadar: true,
        },
      },
    })

    flushPromises()
    await nextTick()
    return vueWrapper
  }
})

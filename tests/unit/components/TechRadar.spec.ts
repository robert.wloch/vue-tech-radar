import {flushPromises, mount, VueWrapper} from '@vue/test-utils'
import {ComponentPublicInstance, nextTick} from 'vue'
import sut from '@/components/TechRadar.vue'

describe('TechRadar', () => {
  let wrapper: VueWrapper<ComponentPublicInstance> | null = null

  afterEach(() => {
    if (wrapper) {
      wrapper.unmount()
    }
  })

  it('should mount the tech radar component', async () => {
    wrapper = await createWrapper()

    expect(wrapper.exists()).toBeTruthy()
  })

  async function createWrapper(): Promise<VueWrapper> {
    const vueWrapper = mount(sut)

    flushPromises()
    await nextTick()
    return vueWrapper
  }
})
